;; This is an operating system configuration generated
;; by the graphical installer.
;;
;; Once installation is complete, you can learn and modify
;; this file to tweak the system configuration, and pass it
;; to the 'guix system reconfigure' command to effect your
;; changes.


;; Indicate which modules to import to access the variables
;; used in this configuration.
(use-modules (gnu)
	     (gnu packages shells)
	     (nongnu packages linux)
	     (nongnu system linux-initrd))
(use-service-modules cups desktop networking base ssh sddm xorg avahi dbus)

(operating-system
  (kernel linux)
  (initrd microcode-initrd)
  (firmware (list linux-firmware sof-firmware))
  (locale "en_US.utf8")
  (timezone "Asia/Taipei")
  (keyboard-layout (keyboard-layout "tw"))
  (host-name "ANB297")

  ;; The list of user accounts ('root' is implicit).
  (users (cons* (user-account
                  (name "stuart")
                  (comment "Stuart Wu")
                  (group "users")
                  (home-directory "/home/stuart")
		  (shell (file-append zsh "/bin/zsh"))
                  (supplementary-groups '("wheel" "netdev" "audio" "video" "lp" "kvm" "input" "tty")))
                %base-user-accounts))

  ;; Packages installed system-wide.  Users can also install packages
  ;; under their own account: use 'guix search KEYWORD' to search
  ;; for packages and 'guix install PACKAGE' to install a package.
  (packages (append (specifications->packages (list "sway"
						    "waybar"
						    "i3-autotiling"
						    "dmenu"
						    "alacritty" 
						    "nss-certs"
						    "emacs-no-x"
						    "qtbase"
						    "qtwayland"))
		    %base-packages))

  ;; Below is the list of system services.  To search for available
  ;; services, run 'guix system search KEYWORD' in a terminal.
  (services
   (append (list

            ;; To configure OpenSSH, pass an 'openssh-configuration'
            ;; record as a second argument to 'service' below.
            (service openssh-service-type)
	    (service sddm-service-type
		     (sddm-configuration
       		      (display-server "wayland")
		      (auto-login-user "stuart")
		      (auto-login-session "sway.desktop")))
            (service cups-service-type)
	    (service bluetooth-service-type)
	    polkit-wheel-service)

   (modify-services %desktop-services
             (guix-service-type config => (guix-configuration
               (inherit config)
               (substitute-urls
                (append (list "https://substitutes.nonguix.org")
                  %default-substitute-urls))
               (authorized-keys
                (append (list (local-file "/etc/guix/nonguix.pub"))
			%default-authorized-guix-keys))))
	     (delete gdm-service-type))))

  (bootloader (bootloader-configuration
                (bootloader grub-efi-removable-bootloader)
                (targets (list "/boot/efi"))
                (keyboard-layout keyboard-layout)))

  ;; The list of file systems that get "mounted".  The unique
  ;; file system identifiers there ("UUIDs") can be obtained
  ;; by running 'blkid' in a terminal.
  (file-systems (cons* (file-system
                         (mount-point "/")
                         (device (uuid
                                  "116e363e-9fa0-4cf4-acba-393e412d7891"
                                  'ext4))
                         (type "ext4"))
                       (file-system
                         (mount-point "/boot/efi")
                         (device (uuid "D952-92C5"
                                       'fat32))
                         (type "vfat")) %base-file-systems)))
