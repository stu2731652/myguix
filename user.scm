;; This "home-environment" file can be passed to 'guix home reconfigure'
;; to reproduce the content of your profile.  This is "symbolic": it only
;; specifies package names.  To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(use-modules (gnu home)
             (gnu packages)
             (gnu services)
	     (gnu home services)
             (guix gexp)
	     (gnu packages terminals)
	     (gnu packages suckless)
	     (nongnu packages mozilla)
             (gnu home services shells)
	     (mysrv locale))

(home-environment
  ;; Below is the list of packages that will show up in your
  ;; Home profile, under ~/.guix-home/profile.
  (packages (specifications->packages (list "firefox"
					    "rdesktop"
					    "oxygen-icons"
					    "hicolor-icon-theme"
					    "font-fira-mono"
					    "font-fira-go"
					    "font-fira-code"
					    "font-fira-sans"
					    "font-arphic-ukai"
					    "font-cns11643"
					    "font-cns11643-swjz"
					    "font-adobe-source-han-sans"
					    "font-adobe-source-code-pro"
					    "font-adobe-source-serif-pro"
                                            "pavucontrol-qt"
                                            "falkon"
                                            "pipewire")))

  ;; Below is the list of Home services.  To search for available
  ;; services, run 'guix home search KEYWORD' in a terminal.
  (services
   (list (simple-service 'my-env-vars-service
			 home-environment-variables-service-type
			 `(("EDITOR" . ,(file-append emacs "/bin/emacs -nw"))
			   ("BROWSER" . ,(file-append firefox "/bin/firefox"))
			   ("TERM" . , "alacritty")
			   ("MENU" . ,(file-append dmenu "/bin/dmenu_run"))
			   ("MOZ_ENABLE_WAYLAND" . "1")
			   ("QT_QPA_PLATFORM" . "xcb")
			   ("GDK_BACKEND" . "x11")
			   ("TZ" . "Asia/Taipei")))
	 (service home-zsh-service-type (home-zsh-configuration))
	 (service home-fcitx5-service-type))))
