(define-module (mysrv locale)
  #:use-module (gnu services configuration)
  #:use-module (gnu home services)
  #:use-module (gnu packages fcitx5)
  #:use-module (gnu packages)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (guix gexp)

  #:export (home-fcitx5-service-type
	    home-fcitx5-configuration
	    home-fcitx5-configuration?
	    home-fcitx5-extension))

(define fcitx5-default-env
  '(("GTK_IM_MODULE" . "fcitx")
    ("QT_IM_MODULE" . "fcitx")
    ("XMODIFIERS=@im" . "fcitx")
    ("GLFW_IM_MODULE" . "fcitx")
    ("SDL_IM_MODULE" . "fcitx")
    ("GUIX_GTK2_IM_MODULE_FILE" . "/run/current-system/profile/lib/gtk-2.0/2.10.0/immodules-gtk2.cache")
    ("GUIX_GTK3_IM_MODULE_FILE" . "/run/current-system/profile/lib/gtk-3.0/3.0.0/immodules-gtk3.cache")))


(define-record-type* <home-fcitx5-configuration>
  home-fcitx5-configuration
  make-home-fcitx5-configuration
  home-fcitx5-configuration?
  (inputs home-fcitx5-configuration-inputs
	  (default '()))
  (env home-fcitx5-configuration-env
       (default fcitx5-default-env)))

(define (add-fcitx5-packages config)
  (append config '(fcitx5 fcitx5-gtk fcitx5-gtk4 fcitx5-qt fcitx5-configtool)
	home-fcitx5-configuration-inputs))

(define (fcitx5-shepherd-service config)
  (list (shepherd-service
	 (documentation "Start Fcitx5 input-method.")
	 (provision '(fcitx5))
	 (start #~(make-forkexec-constructor
		   (list (file-append fcitx5 "/bin/fcitx5")
			 "-D" "-k")))
	 (stop #~(make-kill-destructor)))))

(define home-fcitx5-service-type
  (service-type (name 'home-fcitx5)
		(extensions (list (service-extension
				   home-profile-service-type
				   add-fcitx5-packages)
				  (service-extension
				   home-environment-variables-service-type
				   home-fcitx5-configuration-env)
				  (service-extension
				   shepherd-root-service-type
				   fcitx5-shepherd-service)))
		(default-value (home-fcitx5-configuration))
		(description "Install and configure Fcitx5")))
