(define-module (mysrv desktop)
  #:use-module (gnu services configuration)
  #:use-module (gnu home services)
  #:use-module (gnu packages)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (guix packages)
  #:use module (guix gexp)

  #:export (home-sway-service-type))

(define (add-wayland-packages config)
  (append config '(qtwayland qtwayland-5 egl-wayland wayland-utils waypipe wayvnc)))

(define (add-wayland-default-env config)
  (append config `(("MOZ_ENABLE_WAYLAND" . "1")
		   ("QT_QPA_PLATFORM" . "wayland;xcb")
		   ("GDK_BACKEND" . "wayland")
		   ("CLUTTER_BACKEND" . "wayland")
		   ("SDL_VIEDODRIVER" . "wayland")
		   ("WINIT_UNIX_BACKEND" . "wayland"))))

(define (add-appbind-setup config)
  (for-each
   (lambda (p)
     (let ((k (car p) (v (cdr p))))
       (string-append "set $" k " " v))) config))

(define home-appbind-service-type
  (service-type (name 'home-appbind)
		(extensions
		 (list (service-extension
			home-service-type
			add-appbind-setup))
		 (compose concatenate)
		 (extend append)
		 (default-value '())
		 (description "Set the default apps for categories."))))

(define wayland-service-type
  (service-type (name 'root-wayland)
		(extensions (list (service-extension
				   home-profile-service-type
				   add-wayland-packages)
				  (service-extension
				   home-environment-variables-service-type
				   add-wayland-default-env)))))

(define-record-type* <home-myfonts-configuration>
  (service-type (name 'home-myfonts)
		(extension)))

(define-record-type* <home-sway-configuration>)

(define home-sway-service-type
  (service-type (name 'home-sway)
		(extension)))
