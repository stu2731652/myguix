(define-module (mypkg dev)
  #:use-module (gnu packages)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages tree-sitter)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages rust)
  #:use-module (gnu packages rust-apps)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils))

(define-public rust-str-indices-0.4
  (package
   (name "rust-str-indices")
   (version "0.4.0")
   (source (origin (method url-fetch)
		   (uri (crate-uri "str_indices" version))
		   (file-name (string-append name "-" version ".tar.gz"))
		   (sha256
		    (base32 "1h7jl3j0q1ip9gbmiwrkr2x2w1i0lms47s0bc9sf05y8h3x9k4cx"))))
   (build-system cargo-build-system)
   (arguments `(#:tests? #false
		#:skip-build? #t
		#:cargo-development-inputs
		(("rust-proptest", rust-proptest-1)
		 ("rust-criterion", rust-criterion-0.3))))
   (home-page "https://github.com/cessen/str_indices")
   (synopsis "Count and convert between various ways of indexing utf8 string slices.")
   (description "This package provides extra indexing schemes for utf8 string slices.")
   (license (list license:asl2.0 license:expat))))

(define-public rust-ropey-1.6
  (package
   (name "rust-ropey")
   (version "1.6.0")
   (source (origin (method url-fetch)
		   (uri (crate-uri "ropey" version))
		   (file-name (string-append name "-" version ".tar.gz"))
		   (sha256
		    (base32 "1vbiqqh6zyqimfy2945lnixi8c8v4n05lg73cvb50bm38cn7mkjk"))))
   (build-system cargo-build-system)
   (arguments `(#:tests? #false
		#:skip-build? #t
		#:cargo-inputs
		(("rust-smallvec", rust-smallvec-1)
		 ("rust-str-indices", rust-str-indices-0.4))
		#:cargo-development-inputs
		(("rust-rand", rust-rand-0.8)
		 ("rust-proptest", rust-proptest-1)
		 ("rust-criterion", rust-criterion-0.3)
		 ("rust-unicode-segmentaion", rust-unicode-segmentation-1)
		 ("rust-fnv", rust-fnv-1)
		 ("rust-fxhash", rust-fxhash-0.2))))
   (home-page "https://github.com/cessen/ropey")
   (synopsis "A utf8 text rope for manipulating and editing large texts.")
   (description "Ropey is a UTF-8 text rope for Rust, designed to be the backing text-buffer for applications such as text editors. Ropey is fast, robust, and can handle huge texts and memory-incoherent edits with ease.")
   (license license:expat)))

(define-public rust-unicode-general-category-0.6
  (package
   (name "rust-unicode-general-category")
   (version "0.6.0")
   (source (origin (method url-fetch)
		   (uri (crate-uri "unicode-general-category" version))
		   (file-name (string-append name "-" version ".tar.gz"))
		   (sha256
		    (base32 "1rv9715c94gfl0hzy4f2a9lw7i499756bq2968vqwhr1sb0wi092"))))
   (build-system cargo-build-system)
   (arguments `(#:tests #false
		#:skip-build? #t))
   (home-page "https://github.com/yeslogic/unicode-general-category")
   (synopsis "Fast lookup of the Unicode General Category property for @code{char} in Rust.")
   (description "Fast lookup of the Unicode General Category property for char in Rust using Unicode 15.0 data. This crate is no-std compatible.")
   (license license:asl2.0)))

(define-public rust-etcetera-0.8
  (package
   (name "rust-etcetera")
   (version "0.8.0")
   (source (origin (method url-fetch)
		   (uri (crate-uri "etcetera" version))
		   (file-name (string-append name "-" version ".tar.gz"))
		   (sha256
		    (base32 "0hxrsn75dirbjhwgkdkh0pnpqrnq17ypyhjpjaypgax1hd91nv8k"))))
   (build-system cargo-build-system)
   (arguments `(#:test? #false
		#:skip-build? #t
		#:cargo-inpus
		(("rust-cfg-if", rust-cfg-if-1)
		 ("rust-home", rust-home-0.5))))
   (home-page "https://github.com/lunacookies/etcetera")
   (synopsis "An unopinionated library for obtaining configuration, data, cache, & other directories")
   (description "An unopinionated Rust library for locating configuration, data and cache directories across platforms")
   (license (list license:asl2.0 license:expat))))

(define-public helix
  (package
   (name "helix")
   (version "23.05")
   (source (origin (method git-fetch)
		   (uri (git-reference (url "https://github.com/helix-editor/helix")
				       (commit version)))
		   (file-name (git-file-name name version))
		   (sha256
		    (base32 "0g1ckbfjg26fhspm0hiw6nhqy333z1d9qs3kwh5krgabsrd6xkss"))))
   (build-system cargo-build-system)
   (arguments `(#:tests? #false
		#:cargo-inputs
		(("rust-ropey", rust-ropey-1.6)
		 ("rust-smallvec", rust-smallvec-1)
		 ("rust-smartstring", rust-smartstring-1)
		 ("rust-unicode-segmentation", rust-unicode-segmentation-1)
		 ("rust-unicode-width", rust-unicode-width-0.1)
		 ("rust-unicode-general-category", rust-unicode-general-category-0.6)
		 ("rust-slotmap", rust-slotmap-1)
		 ("tree-sitter-rust", tree-sitter-rust)
		 ("rust-once-cell", rust-once-cell-1)
		 ("rust-arc-swap", rust-arc-swap-1)
		 ("rust-regex", rust-regex-1)
		 ("rust-bitflags", rust-bitflags-2)
		 ("rust-ahash", rust-ahash-0.3)
		 ("rust-hashbrown", rust-hashbrown-0.13)
		 ("rust-dunce", rust-dunce-1)
		 ("rust-log", rust-log-0.4)
		 ("rust-serde", rust-serde-1)
		 ("rust-serde-json", rust-serde-json-1)
		 ("rust-toml", rust-toml-0.7)
		 ("rust-imara-diff", rust-imara-diff-0.1)
		 ("rust-encoding-rs", rust-encoding-rs-0.8)
		 ("rust-chrono", rust-chrono-0.4)
		 ("rust-etcetera", rust-etcetera-0.8)
		 ("rust-textwrap", rust-textwrap-0.16))
		#:cargo-development-inputs
		(("rust-quickcheck", rust-quickcheck-1)
		 ("rust-indoc", rust-indoc-2))))
   (home-page "https://helix-editor.com/")
   (synopsis "A post-modern modal text editor.")
   (description "A Kakoune / Neovim inspired editor, written in Rust.")
   (license license:mpl2.0)))
