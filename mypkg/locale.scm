(define-module (mypkg locale)
  #:use-module (guix licenses)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gettext)
  #:use-module (guix i18n)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages fcitx5)
  #:use-module (gnu packages language)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26))

(define-public fcitx5-chewing
  (package
   (name "fcitx5-chewing")
   (version "5.1.0")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://download.fcitx-im.org/fcitx5"
				"/fcitx5-chewing/fcitx5-chewing-" version
				".tar.xz"))
            (sha256
	     (base32 "0d9qsggp53v7l07381cih9074bdkj2kq0xf52ixprpqds3y1dkp0"))))
   (build-system cmake-build-system)
   (arguments `(#:tests? #f))
   (inputs (list fcitx5 libchewing))
   (native-inputs (list gettext-minimal extra-cmake-modules pkg-config))
   (home-page "https://github.com/fcitx/fcitx5-chewing")
   (synopsis "Chewing Input Method Support for Fcitx 5")
   (description
    "@dfn{fcitx5-chewing} provides the libchewing input method support for fcitx5.")
   (license lgpl2.1+)))
